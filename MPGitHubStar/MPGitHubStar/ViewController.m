//
//  ViewController.m
//  MPGitHubStar
//
//  Created by mopellet on 2017/6/9.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import "ViewController.h"
#import "MPExecuteManager.h"
#import "MPDataBaseManager.h"
#import "MonitorFileChangeHelp.h"
#import "MPCommon.h"
@interface ViewController()<NSComboBoxDataSource, NSTableViewDataSource>

@property (weak) IBOutlet NSTextField *starCountTextField;
@property (weak) IBOutlet NSTextField *registerCountTextField;
@property (weak) IBOutlet NSButton *startStarButton;
@property (weak) IBOutlet NSButton *startRegisterButton;
@property (weak) IBOutlet NSScrollView *logView;
@property (unsafe_unretained) IBOutlet NSTextView *textView;
@property (weak) IBOutlet NSComboBox *urlComboBox;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSTextField *dbPathTextField;
@property (weak) IBOutlet NSTextField *delayTextField;
@property (weak) IBOutlet NSTextField *versionLabel;

@property (nonatomic, assign) BOOL execute;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 待做任务
    // 随机密码
    // 暂停任务的功能
    // 数据库路径可配置的问题
 
    [self setupUI];
    [self setupSetting];
}

- (void) setupSetting {
    [MPDataBaseManager sharedManager];
    self.starCountTextField.stringValue = @(2).stringValue;
    dispatch_queue_t _queue = dispatch_queue_create("write", DISPATCH_QUEUE_CONCURRENT);
    NSString *logFilePath = [MPResourcePath stringByAppendingPathComponent:@"log.log"];
#ifdef DEBUG
        logFilePath = [MPResourcePath stringByAppendingPathComponent:@"log.log"];
#else
        logFilePath = [[MPCommon getUserDBPath] stringByAppendingPathComponent:@"log.log"];
#endif
    dispatch_barrier_async(_queue, ^{
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        [defaultManager removeItemAtPath:logFilePath error:nil];
        freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stdout);
        freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
    });
    
    [[[MonitorFileChangeHelp alloc] init] watcherForPath:logFilePath block:^(NSInteger type) {
        NSString *string = [NSString stringWithContentsOfFile:logFilePath encoding:NSUTF8StringEncoding error:nil];
        _textView.string = string;
        [self scrollProgressTextViewToEnd];
    }];
    
    self.urlComboBox.usesDataSource = YES;
    self.urlComboBox.dataSource = self;
    [self.urlComboBox reloadData];
    if ([MPCommon allRepositoryURLs].count) {
        [self.urlComboBox selectItemAtIndex:0];
    }
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    self.tableView.headerView = nil;
    
    self.delayTextField.stringValue = @([MPCommon getDelay]).stringValue;
    
#ifdef DEBUG
        self.dbPathTextField.stringValue = MPResourcePath;
        [[MPDataBaseManager sharedManager] setupDataBaseDefault];
#else
        self.dbPathTextField.stringValue = [MPCommon getUserDBPath];
        [[MPDataBaseManager sharedManager] setupDataBaseWith:[MPCommon getUserDBPath]];
#endif
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.stringValue = [NSString stringWithFormat:@"version：%@",version];
}


- (void) setupUI {
    [_logView setBorderType:NSNoBorder];
    [_logView setHasVerticalScroller:YES];
    [_logView setHasHorizontalScroller:NO];
    [_logView setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
    [_textView setVerticallyResizable:YES];
    [_textView setHorizontallyResizable:NO];
    [_textView setFont:[NSFont fontWithName:@"Helvetica" size:12.0]];
    [_textView setEditable:NO];
    
}

- (void)scrollProgressTextViewToEnd
{
    if ([_textView isFlipped])
        [_textView scrollPoint:NSMakePoint(0.0, NSMaxY([_textView frame]) - NSHeight([_textView visibleRect]))];
    else
        [_textView scrollPoint:NSMakePoint(0.0, 0.0)];
}

#pragma mark - NSComboBoxDataSource

- (NSInteger)numberOfItemsInComboBox:(NSComboBox *)comboBox {
    return [MPCommon allRepositoryURLs].count;
}
- (nullable id)comboBox:(NSComboBox *)comboBox objectValueForItemAtIndex:(NSInteger)index
{
    NSArray *urls =[MPCommon allRepositoryURLs];
    return urls[index];
}

- (NSUInteger)comboBox:(NSComboBox *)comboBox indexOfItemWithStringValue:(NSString *)string {
    NSArray *urls =[MPCommon allRepositoryURLs];
    return [urls indexOfObject:string];
}

//返回表格的行数
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView;
{
    return [[MPDataBaseManager sharedManager] getAllUserModel].count;
}

//用了下面那个函数来显示数据就用不上这个，但是协议必须要实现，所以这里返回nil
- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    MPUserModel *userModel = [[MPDataBaseManager sharedManager] getAllUserModel][row];
    NSString *identifier = [tableColumn identifier];
    // 1.1.判断是哪一列
    if ([identifier isEqualToString:@"serial"]) {
        return @(row).stringValue;
    }
    else if ([identifier isEqualToString:@"email"]) {
        return userModel.email;
    }else if ([identifier isEqualToString:@"password"]) {
        return  userModel.password;
    }
    return nil;
}

- (IBAction)refreshTableAction:(id)sender {
    [self.tableView reloadData];
}


- (IBAction)onEnter:(id)sender{
    NSTextField *textField = (NSTextField *)sender;
    if(textField.tag == 1) {
        // 更改
        [MPCommon setDelay:[self.delayTextField.stringValue integerValue]];
        self.delayTextField.stringValue = @([MPCommon getDelay]).stringValue;
    }
    else if(textField.tag == 2){
        // 添加保存URL到偏好设置并刷新
        if (!self.urlComboBox.stringValue.length) {
            return;
        }
        if (![self.urlComboBox.stringValue hasPrefix:@"https://github.com/"])
        {
            return;
        }
        
        NSString *url = [self.urlComboBox.stringValue copy];
        NSString* abUrl = [url stringByReplacingOccurrencesOfString:@"https://github.com/" withString:@""];
        if (![[abUrl componentsSeparatedByString:@"/"] lastObject].length) {
            return;
        }
        
        
        [MPCommon addRepositoryURL:self.urlComboBox.stringValue];
        [self.urlComboBox reloadData];
    }
}

//static BOOL execute = NO;

- (IBAction)startSatrAction:(NSButton *)sender {
    if (![[MPDataBaseManager sharedManager] getAllUserModel].count) {
        [MPCommon alertShowInWindow:@"Please register some accout."];
        return;
    }
    
    NSString *url = [self.urlComboBox.stringValue lowercaseString];
    NSInteger startCount = [self.starCountTextField.stringValue integerValue];
    
    
#ifdef DEBUG
    
#else
    if([url containsString:@"mopellet"])
    {
        [MPCommon alertShowInWindow:@"请选择正确的URL"];
        return;
    }
#endif
    
    if (_execute) {
        NSLog(@"正在执行");
        return;
    }
    
    if (url.length && startCount) {
        _execute = YES;
        [MPExecuteManager mp_executeMultitaskStar:startCount urlString:url progress:^(float progress) {
            NSLog(@"base:%f",progress);
        } complete:^(BOOL success, NSError *error) {
            if(success)
            {
                NSLog(@"execute success");
            }
            else
            {
                 NSLog(@"error:%@",error);
            }
            _execute = NO;
            [MPCommon alertShowInWindow:@"execute finish"];
        }];
    }
}

- (IBAction)startRegisterAction:(NSButton *)sender {
    
    NSInteger registerCount = [self.registerCountTextField.stringValue integerValue];
    
    if (_execute) {
        NSLog(@"正在执行");
        return;
    }
    
    if (registerCount) {
        _execute = YES;
        [MPExecuteManager mp_executeMultitaskRegister:registerCount progress:^(float progress) {
            NSLog(@"base:%f",progress);
        } complete:^(BOOL success, NSError *error) {
            if(success)
            {
                NSLog(@"execute success");
            }
            else
            {
                NSLog(@"error:%@",error);
            }
            _execute = NO;
            [MPCommon alertShowInWindow:@"execute finish"];
        }];
    }
}

- (IBAction)openUrlAction:(id)sender {
    if (!self.urlComboBox.stringValue.length) {
        return;
    }
    if (![self.urlComboBox.stringValue hasPrefix:@"https://github.com/"])
    {
        return;
    }
    
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:self.urlComboBox.stringValue]];
}


- (IBAction)saveDBPathAction:(id)sender {
    if (_execute) {
        NSLog(@"正在执行");
        return;
    }
    if(!self.dbPathTextField.stringValue.length)
    {
        return;
    }
    
    [MPCommon setUserDBPath:self.dbPathTextField.stringValue];
    
#ifdef DEBUG
        [[MPDataBaseManager sharedManager] setupDataBaseDefault];
#else
        [[MPDataBaseManager sharedManager] setupDataBaseWith:[MPCommon getUserDBPath]];
#endif
    [self.tableView reloadData];
}

- (IBAction)gotohomepage:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://github.com/MoPellet"]];
}

- (IBAction)gotogithub:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://github.com/MoPellet/FuckGitHub"]];
}



@end
