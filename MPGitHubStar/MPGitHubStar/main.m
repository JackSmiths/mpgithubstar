//
//  main.m
//  MPGitHubStar
//
//  Created by mopellet on 2017/6/9.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
