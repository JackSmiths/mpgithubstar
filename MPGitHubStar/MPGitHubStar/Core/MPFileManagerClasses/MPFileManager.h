
//
//  Created by mopellet on 17/3/8.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import <Foundation/Foundation.h>



typedef NS_ENUM(NSInteger, MPFileManagerWriteMode) {
    MPFileManagerWriteModeAdd,
    MPFileManagerWriteModeCover
};

@interface MPFileManager : NSObject

+ (instancetype)shareInstance;

/**
 *  异步读文件
 *
 *  @param path      文件完整路径包含后缀名
 *  @param complete  文件信息
 */
- (void)readFile:(NSString *)path
        complete:(void (^)(NSData *data))complete;


/**
 *  异步写文件
 *
 *  @param path      文件完整路径
 *  @param data      需要写入的文件信息
 *  @param writeMode 写入模式追加或覆盖
 *  @param complete  完成的回调（YES=成功，NO=失败）
 */
- (void)writeFile:(NSString *)path
             data:(NSData *)data
        writeMode:(MPFileManagerWriteMode)writeMode
         complete:(void (^)(BOOL result))complete;

@end
