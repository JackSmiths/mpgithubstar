//
//  MPNetworkHelper.m
//  MPGitHubStar
//
//  Created by mopellet on 2017/6/9.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import "MPNetworkHelper.h"

#import <AFNetworking.h>
#import "HTMLParser.h"
#import "MPDataBaseManager.h"
NSString * const MPGetRegisterFirstUrl = @"https://github.com/join?source=login";
NSString * const MPPostRegisterSecondUrl = @"https://github.com/join";
NSString * const MPGetGitHubLoginUrl = @"https://github.com/login";
NSString * const MPGitHubHost = @"https://github.com";
NSString * const MPGitHubStarSuffix = @"/star";
NSString * const MPGitHubUnStarSuffix = @"/unstar";
NSString * const MPGitHubAllowFlagUrl = @"https://raw.githubusercontent.com/MoPellet/FuckGitHub/master/flag";

@interface MPNetworkHelper()
@property (nonatomic, readwrite, strong) AFHTTPSessionManager *manager;
@property (assign, nonatomic, getter=isAllow) BOOL allow;
@end

@implementation MPNetworkHelper

static id _instance = nil;

+ (id)allocWithZone:(struct _NSZone*)zone {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _instance= [super allocWithZone:zone];
        
    });
    
    return _instance;
    
}

+ (instancetype) shareInstance {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _instance = [[[self class] alloc] init];
        
    });
    
    return _instance;
    
}

- (id)copyWithZone:(NSZone*)zone {
    
    return _instance;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _allow = [[NSString stringWithContentsOfURL:[NSURL URLWithString:MPGitHubAllowFlagUrl] encoding:NSUTF8StringEncoding error:nil] boolValue];
    }
    return self;
}

- (void) startSingleRegisterComplete:(void(^)(BOOL success ,NSError *error))complete
{
    if (_allow) {
        MPUserModel *model = [MPUserModel random];
        [self startRegister:model complete:complete];
    }
    else {
        complete(NO, [NSError errorWithDomain:@"no network or no allow" code:0 userInfo:nil]);
    }

}

/** 单个注册*/
- (void) startRegister:(MPUserModel *)parameter  complete:(void(^)(BOOL success ,NSError *error))complete{

    AFHTTPSessionManager *manager = [[self class] currentManager];
    
    [[self class] removeAllCookies];
    
    [manager GET:MPGetRegisterFirstUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSError *error = nil;
        HTMLParser *parser = [[HTMLParser alloc] initWithString:result error:&error];
        HTMLNode *bodyNode = [parser body];
        
        NSArray *formNodes = [bodyNode findChildTags:@"form"];
        
        NSString *authenticity_token;
        NSString *timestamp;
        NSString *timestamp_secret;
        
        for (HTMLNode *formNode in formNodes) {
            if ([[formNode getAttributeNamed:@"action"] isEqualToString:@"/join"]) {
                NSArray *inputNodes = [formNode findChildTags:@"input"];
                for (HTMLNode *inputNode in inputNodes) {
                    if ([[inputNode getAttributeNamed:@"name"] isEqualToString:@"authenticity_token"]) {
                        authenticity_token =  [inputNode getAttributeNamed:@"value"];
                        continue;
                    }
                    else
                        if ([[inputNode getAttributeNamed:@"name"] isEqualToString:@"timestamp"]) {
                            timestamp =  [inputNode getAttributeNamed:@"value"];
                            continue;
                        }
                        else if ([[inputNode getAttributeNamed:@"name"] isEqualToString:@"timestamp_secret"]) {
                            timestamp_secret =  [inputNode getAttributeNamed:@"value"];
                            continue;
                        }
                    
                }
                break;
            }
        }
        
        NSDictionary *registerParameter = @{@"utf8":@"✓",
                                            @"authenticity_token":authenticity_token,
                                            @"user[login]":parameter.username,
                                            @"user[email]":parameter.email,
                                            @"user[password]":parameter.password,
                                            @"source":@"login",
                                            @"timestamp":timestamp,
                                            @"timestamp_secret":timestamp_secret};
        [manager POST:MPPostRegisterSecondUrl parameters:registerParameter progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSString *str = @"Welcome to GitHub";
            //在str1这个字符串中搜索\n，判断有没有
            if ([result rangeOfString:str].location != NSNotFound) {
                [parameter saveSuccessModel];
                complete(YES, nil);
            }
            else
            {
                NSLog(@"注册失败");
                complete(NO, nil);
                [parameter saveRegisterFailModel];
            }
            //
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            complete(NO, error);
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NO, error);
    }];
}


- (void) startStarUrlString:(NSString *)urlString
                   Complete:(void(^)(BOOL success ,NSError *error))complete
{
     if (_allow) {
    [[MPDataBaseManager sharedManager] getUserModelByUrlString:urlString complete:^(MPUserModel *userModel, NSError *error) {
        if(userModel)
        {
            NSLog(@"beginStar:%@",userModel);
            [self startStar:userModel urlString:urlString complete:complete];
        }
        else
        {
            complete(NO,error);
        }
    }];
     }
     else
     {
     complete(NO, [NSError errorWithDomain:@"no network or no allow" code:0 userInfo:nil]);
     }
}


- (void) startStar:(MPUserModel *)parameter
         urlString:(NSString *)urlString
          complete:(void(^)(BOOL success ,NSError *error))complete {
    
    AFHTTPSessionManager *manager = [[self class] currentManager];
    
    [[self class] removeAllCookies];
    
    
    [manager GET:MPGetGitHubLoginUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        //        NSLog(@"%@",result);
        NSError *error = nil;
        HTMLParser *parser = [[HTMLParser alloc] initWithString:result error:&error];
        HTMLNode *bodyNode = [parser body];
        
        NSArray *formNodes = [bodyNode findChildTags:@"form"];
        
        for (HTMLNode *formNode in formNodes) {
            if ([[formNode getAttributeNamed:@"action"] isEqualToString:@"/session"]) {
                NSArray *inputNodes = [formNode findChildTags:@"input"];
                for (HTMLNode *inputNode in inputNodes) {
                    if ([[inputNode getAttributeNamed:@"name"] isEqualToString:@"authenticity_token"]) {
//                        NSLog(@"%@", [inputNode getAttributeNamed:@"value"]);
                        NSString *login_token = [inputNode getAttributeNamed:@"value"];
                        //                        commit	Sign in
                        //                        utf8	✓
                        //                        authenticity_token	Dk6iXgCxjQRYpuJDn+N/Tl69OJtffiiYQ2yhxEfXUhhejLJ/MVCe5/LJGrzcVaHHPgvjWL0Kc8Dsmp24ttWMsw==
                        //                        login	mopellet
                        //                        password	cnm1314520
                        NSDictionary *dictionary = @{@"commit":@"Sign in",
                                                     @"utf8":@"✓",
                                                     @"authenticity_token":login_token,
                                                     @"login":parameter.email,
                                                     @"password":parameter.password
                                                     };
//                        NSLog(@"%@",dictionary); // You are being
                        [manager POST:@"https://github.com/session" parameters:dictionary progress:^(NSProgress * _Nonnull uploadProgress) {
                            
                        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                            if(result){
//                                https://github.com/MoPellet/MPVPNManager
                                NSString *actionString = [[urlString stringByReplacingOccurrencesOfString:MPGitHubHost withString:@""]
                                                  stringByAppendingString:MPGitHubStarSuffix];
                                
                                [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
                                    
                                } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                    NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                                    NSError *error = nil;
                                    HTMLParser *parser = [[HTMLParser alloc] initWithString:result error:&error];
                                    HTMLNode *bodyNode = [parser body];
                                    
                                    NSArray *formNodes = [bodyNode findChildTags:@"form"];
                                    for (HTMLNode *formNode in formNodes) {
//                                        /MoPellet/MPVPNManager/star
                                        if ([[formNode getAttributeNamed:@"action"] isEqualToString:actionString]) {
                                            NSArray *inputNodes = [formNode findChildTags:@"input"];
                                            for (HTMLNode *inputNode in inputNodes) {
                                                if ([[inputNode getAttributeNamed:@"name"] isEqualToString:@"authenticity_token"]) {
                                                    NSString *star_token = [inputNode getAttributeNamed:@"value"];
//                                                    NSLog(@"star_token:%@",star_token);
                                                    //                                                     utf8	✓
                                                    //                                                     authenticity_token	hL1JCXg2412Dk1WQiMf7mip94wJTGWlxsc3FIyegvSjjqsyI7F/sy6tWq8WlPAfgGPDe5v/xTLAYvNlJ1aUcHg==
                                                    NSDictionary *parts = @{@"utf8":@"✓",
                                                                            @"authenticity_token":star_token};
//                                                    NSLog(@"parts:%@",parts);
                                                    // 加入这个很重要
                                                    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
                                                    manager.responseSerializer = [AFJSONResponseSerializer serializer] ;
                                                    NSString * starUrlString = [urlString stringByAppendingString:@"/star"];
//                                                    @"https://github.com/MoPellet/MPVPNManager/star"
                                                    [manager POST:starUrlString parameters:parts progress:^(NSProgress * _Nonnull uploadProgress) {
                                                        
                                                    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                                                        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
                                                        //                                                         statusCode(responses.statusCode);
//                                                        NSLog(@"statusCode:%ld",responses.statusCode );
                                                        //                                                        NSLog(@"%@",responseObject);
                                                        NSDictionary *dict = responseObject;
                                                        NSLog(@"%@",dict);
//                                                        {
//                                                            count = 46;
//                                                        }
                                                        
                                                        if ([[dict allKeys] containsObject:@"count"])
                                                        {
                                                            // 只需要知道email和url地址就可以了
                                                            NSLog(@"star success");
                                                            
                                                            [[MPDataBaseManager sharedManager] storeBusiness:parameter.email
                                                                                                         url:urlString complete:complete];
                                                        }
                                                        else{
                                                            complete(YES,nil);
                                                        }
                                                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                                                        NSLog(@"%@",error);
                                                         complete(NO,error);
                                                    }];
                                                }
                                            }
                                        }
                                    }
                                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                     complete(NO,error);
                                }];
                            }
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                             complete(NO,error);
                            NSLog(@"ssion:%@",error);
                        }];
                    }
                }
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complete(NO,error);
    }];
}


- (void) getSubRepositories:(NSString *)username
                   complete:(void(^)(NSArray *RepositorieUrls ,NSError *error))complete {
    AFHTTPSessionManager *manager = [[self class] currentManager];
    
    [[self class] removeAllCookies];
    
    NSString *fullURl = [NSString stringWithFormat:@"%@/%@?tab=repositories",MPGitHubHost,username];
    
    [manager GET:fullURl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@",result);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+ (AFSecurityPolicy *) customSecurityPolicy {
    //先导入证书，找到证书的路径
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"https_company" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    
    //AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    //allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    //如果是需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = YES;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
    //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
    //如置为NO，建议自己添加对应域名的校验逻辑。
    securityPolicy.validatesDomainName = NO;
    NSSet *set = [[NSSet alloc] initWithObjects:certData, nil];
    securityPolicy.pinnedCertificates = set;
    
    return securityPolicy;
}

+ (void) removeAllCookies
{
    NSHTTPCookieStorage * cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie * cookie in [cookieStorage cookies])
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
}

+ (AFHTTPSessionManager *)currentManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer] ;
    [manager setSecurityPolicy:[[self class] customSecurityPolicy]];
    return manager;
}

@end
