//
//  MPCommon.m
//  MPGitHubStar
//
//  Created by mopellet on 2017/6/16.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import "MPCommon.h"
#import <AppKit/AppKit.h>
@implementation MPCommon
NSString * const kRepositoryURL = @"RepositoryURL";

+ (void)addRepositoryURL:(NSString *)url {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:kRepositoryURL]) {
        NSArray * urls = [self allRepositoryURLs];
        if ([urls containsObject:url]) {
            return;
        }
        NSMutableArray *mutableUrls = [NSMutableArray arrayWithArray:urls];
//        [mutableUrls addObject:url];
        [mutableUrls insertObject:url atIndex:0];
        [userDefaults setObject:[NSArray arrayWithArray:mutableUrls] forKey:kRepositoryURL];
    }
    else {
        [userDefaults setObject:@[url] forKey:kRepositoryURL];
    }
}

+ (NSArray <NSString *> *)allRepositoryURLs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:kRepositoryURL]) {
        return [userDefaults objectForKey:kRepositoryURL];
    }
    return nil;
}

NSString * const kDelay = @"kDelay";
NSString * const kDBPath = @"kDBPath";


+ (NSString *)getUserDBPath {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kDBPath];
}

+ (BOOL)setUserDBPath:(NSString *)path {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:path forKey:kDBPath];
    return [userDefaults synchronize];
}

+ (BOOL)setDelay:(NSUInteger)delay {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@(delay) forKey:kDelay];
    return [userDefaults synchronize];
}

+ (NSUInteger)getDelay {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kDelay] unsignedIntegerValue];
}

+ (void)load {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths firstObject];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults registerDefaults:@{
                                     kDelay:@(10),
                                     kDBPath:docDir}];
}

+ (void)alertShowInWindow:(NSString *)string {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"close"];
    alert.messageText = string;
    
    [alert beginSheetModalForWindow:[NSApplication sharedApplication].keyWindow completionHandler:^(NSModalResponse returnCode) {
        
    }];
}


@end
