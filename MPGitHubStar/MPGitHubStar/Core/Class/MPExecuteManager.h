

#import <Foundation/Foundation.h>

@interface MPExecuteManager : NSObject

#pragma mark - 开始Star

+ (void)mp_executeMultitaskStar:(NSUInteger)taskCount
                      urlString:(NSString *)urlString
                       progress:(void(^)(float progress))progress
                       complete:(void(^)(BOOL success ,NSError *error))complete;

#pragma mark - 注册账号

+ (void)mp_executeMultitaskRegister:(NSUInteger)taskCount
                           progress:(void(^)(float progress))progress
                           complete:(void(^)(BOOL success ,NSError *error))complete;

+ (void)mp_getSubRepositories:(NSString *)username
                         complete:(void(^)(NSArray *repositorieUrls ,NSError *error))complete;
@end
