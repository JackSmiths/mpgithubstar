//
//  MPCommon.h
//  MPGitHubStar
//
//  Created by mopellet on 2017/6/16.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPCommon : NSObject


+ (void)addRepositoryURL:(NSString *)url;
+ (NSArray <NSString *> *)allRepositoryURLs;

+ (NSString *)getUserDBPath;
+ (BOOL)setUserDBPath:(NSString *)path;

+ (BOOL)setDelay:(NSUInteger)delay;
+ (NSUInteger)getDelay;

+ (void)alertShowInWindow:(NSString *)string;
@end
