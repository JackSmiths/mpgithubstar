//
//  MPNetworkHelper.h
//  MPGitHubStar
//
//  Created by mopellet on 2017/6/9.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPNetworkHelper : NSObject
+ (instancetype)shareInstance;
- (void) startSingleRegisterComplete:(void(^)(BOOL success ,NSError *error))complete;
- (void) startStarUrlString:(NSString *)urlString
                   Complete:(void(^)(BOOL success ,NSError *error))complete;
- (void) getSubRepositories:(NSString *)username
                   complete:(void(^)(NSArray *RepositorieUrls ,NSError *error))complete;
@end
