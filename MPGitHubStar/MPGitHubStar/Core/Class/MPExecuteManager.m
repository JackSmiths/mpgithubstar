
#import "MPExecuteManager.h"
#import "MPExecuteHelper.h"
#import "MPNetworkHelper.h"

#import "MPCommon.h"
@implementation MPExecuteManager

static dispatch_queue_t sub_queue;
static NSUInteger executeDelay;
static NSTimer *executeTimer;
static NSThread *executeThread;

+ (void)initialize
{
    sub_queue = dispatch_queue_create("tk.bourne.testQueue", NULL);
}
#pragma mark - 开始Star

+ (void)mp_executeMultitaskStar:(NSUInteger)taskCount
                      urlString:(NSString *)urlString
                       progress:(void(^)(float progress))progress
                       complete:(void(^)(BOOL success ,NSError *error))complete {

    if (!executeThread) {
        executeThread = [[NSThread alloc] initWithBlock:^{
            __block float totalProgress = 0.0f;
            __block float partProgress = 1.0f / taskCount;
            __block NSUInteger currentTaskIndex = 0;
            
            MPExecuteHelper *helper = [MPExecuteHelper shareInstance];
            __weak typeof(helper) weakHelper = helper;
            helper.complete = ^(BOOL success, NSError *error) {
                currentTaskIndex++;
                totalProgress += partProgress;
                progress(totalProgress);
                if(taskCount == currentTaskIndex)
                {
                    complete(success, error);
                }
                else
                {
                    //                int delay = [[self class] getRandomNumber:1201 to:2000];
                    executeDelay = [MPCommon getDelay];
                    [executeTimer invalidate];
                    executeTimer = nil;
                    executeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f repeats:YES block:^(NSTimer * _Nonnull timer) {
                        executeDelay--;
                        NSLog(@"倒计时：%lu秒",(unsigned long)executeDelay);
                        if (!executeDelay) {
                            [timer invalidate];
                            timer = nil;
                        }
                    }];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(executeDelay * NSEC_PER_SEC)), sub_queue, ^{
                        [[self class] mp_executeTaskStar:currentTaskIndex urlString:urlString complete:weakHelper.complete];
                    });
                }
            };
            
            [[self class] mp_executeTaskStar:currentTaskIndex urlString:urlString complete:weakHelper.complete];
        }];
        [executeThread start];
    }
    else {
        [executeThread cancel];
        executeThread = nil;
        NSLog(@"cancel");
        [NSThread exit];
    }
}


+ (void)mp_executeTaskStar:(NSUInteger)taskIndex
                     urlString:(NSString *)urlString
                      complete:(void(^)(BOOL success ,NSError *error))complete
{
    [[MPNetworkHelper shareInstance] startStarUrlString:urlString Complete:complete];
    
}


#pragma mark - 注册账号
+ (void)mp_executeMultitaskRegister:(NSUInteger)taskCount
                           progress:(void(^)(float progress))progress
                           complete:(void(^)(BOOL success ,NSError *error))complete {
    
    [executeTimer invalidate];
    executeTimer = nil;
    [NSThread detachNewThreadWithBlock:^{
        
        __block float totalProgress = 0.0f;
        __block float partProgress = 1.0f / taskCount;
        __block NSUInteger currentTaskIndex = 0;
        
        MPExecuteHelper *helper = [MPExecuteHelper shareInstance];
        __weak typeof(helper) weakHelper = helper;
        helper.complete = ^(BOOL success, NSError *error) {
            currentTaskIndex++;
            totalProgress += partProgress;
            progress(totalProgress);
            if(taskCount == currentTaskIndex)
            {
                complete(success, error);
            }
            else
            {
//                executeDelay = [[self class] getRandomNumber:1201 to:2000];
                executeDelay = [MPCommon getDelay];
                [executeTimer invalidate];
                executeTimer = nil;
                executeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f repeats:YES block:^(NSTimer * _Nonnull timer) {
                    executeDelay--;
                    NSLog(@"倒计时：%lu秒",(unsigned long)executeDelay);
                    if (!executeDelay) {
                        [timer invalidate];
                        timer = nil;
                    }
                }];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(executeDelay * NSEC_PER_SEC)), sub_queue, ^{
                     NSLog(@"延时之后执行");
                     [[self class] mp_executeTaskRegister:currentTaskIndex complete:weakHelper.complete];
                });
//                [[self class] mp_executeTaskRegister:currentTaskIndex complete:weakHelper.complete];
            }
        };
        
        [[self class] mp_executeTaskRegister:currentTaskIndex complete:weakHelper.complete ];
    }];
}


+ (void)mp_executeTaskRegister:(NSUInteger)taskIndex
              complete:(void(^)(BOOL success ,NSError *error))complete
{
            [[MPNetworkHelper shareInstance] startSingleRegisterComplete:complete];

}

+ (int)getRandomNumber:(int)from to:(int)to

{
    
    return (int)(from + (arc4random() % (to - from + 1)));
    
}
+ (void)mp_getSubRepositories:(NSString *)username
                     complete:(void(^)(NSArray *repositorieUrls ,NSError *error))complete {
    [[MPNetworkHelper shareInstance] getSubRepositories:username complete:complete];
}



@end
