

#import <Foundation/Foundation.h>

@interface MPExecuteHelper : NSObject

@property (nonatomic, copy) void(^complete)(BOOL success ,NSError *error);

+ (instancetype)shareInstance;

@end
