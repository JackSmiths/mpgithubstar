

#import "MPExecuteHelper.h"

@implementation MPExecuteHelper
static id _instance = nil;

+ (id)allocWithZone:(struct _NSZone*)zone {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _instance= [super allocWithZone:zone];
        
    });
    
    return _instance;
    
}

+ (instancetype) shareInstance {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _instance = [[[self class] alloc] init];
        
    });
    
    return _instance;
    
}

- (id)copyWithZone:(NSZone*)zone {
    
    return _instance;
    
}

@end
