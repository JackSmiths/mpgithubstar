//
//  AFJSONResponseSerializer+Serializer.m
//  githubStar
//
//  Created by mopellet on 2017/6/9.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import "AFJSONResponseSerializer+Serializer.h"
#import <objc/runtime.h>
@implementation AFJSONResponseSerializer (Serializer)

+(void)load {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Method ovr_initMethod = class_getInstanceMethod([self class], @selector(init));
        Method swz_initMethod = class_getInstanceMethod([self class], @selector(swizzlingForSetSerializer_init));
        method_exchangeImplementations(ovr_initMethod, swz_initMethod);
    });
    
}

- (id) swizzlingForSetSerializer_init {
    
    id swz_self = [self swizzlingForSetSerializer_init];
    if (swz_self && [swz_self isKindOfClass:[AFJSONResponseSerializer class]]) {
        //start  tiny
        NSSet * contentSet = [NSSet setWithObjects:
                              @"application/json",
                              @"text/json",
                              @"text/javascript",
                              @"text/plain",
                              @"text/html",
                              nil];
        [swz_self setValue:contentSet forKey:@"acceptableContentTypes"];
    }
    else {
        NSLog(@"AFJSONResponseSerializer+Serializer kvc get AFJSONResponseSerializer error");
    }
    //
    return swz_self;
}

@end
