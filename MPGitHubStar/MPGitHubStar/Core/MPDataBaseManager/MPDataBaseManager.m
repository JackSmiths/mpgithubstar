
#import "MPDataBaseManager.h"
#import "MPFileManager.h"
//#define PATH_OF_DOCUMENT [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]


static NSString * const CREATE_USER_TABLE_SQL =
@"CREATE TABLE IF NOT EXISTS MPUserTable (\
userId integer PRIMARY KEY AUTOINCREMENT,\
username TEXT not null,\
email text not null,\
password text not null,\
createtime double not null)\
";

static NSString * const CREATE_URL_TABLE_SQL =
@"CREATE TABLE IF NOT EXISTS MPUrlTable (\
urlId integer PRIMARY KEY AUTOINCREMENT,\
urlString TEXT not null,\
createtime double not null)\
";

static NSString * const CREATE_BUSINESS_TABLE_SQL =
@"CREATE TABLE IF NOT EXISTS MPBusinessTable (\
businessId integer PRIMARY KEY AUTOINCREMENT,\
userId TEXT not null,\
urlId TEXT not null,\
createtime double not null)\
";

NSString * const DB_NAME = @"MPGitHubInfoDB.db";
NSString * const USER_TABLE_NAME = @"MPUserTable";
NSString * const URL_TABLE_NAME = @"MPUrlTable";
NSString * const BUSINESS_TABLE_NAME = @"MPBusinessTable";

static NSString *const INSERT_DATA_SQL = @"INSERT INTO %@ (time, data) values (?, ?)";
static NSString *const INSERT_RANGE_SQL = @"INSERT INTO %@ (startTime, endTime, type) values (?, ?, ?)";
static NSString *const SELECT_ALL_SQL = @"SELECT * from %@";
static NSString *const QUERY_DATA_SQL = @"SELECT * from %@ where time between ? and ?";
static NSString *const DELETE_DATA_SQL = @"DELETE from %@ where time between ? and ?";


/** 主键不用插入 **/
static NSString *const INSERT_USER_TABLE_SQL = @"INSERT INTO MPUserTable (username,email,password,createtime) VALUES(?, ?, ?, ?)";
static NSString *const INSERT_URL_TABLE_SQL = @"INSERT INTO MPUrlTable (urlString,createtime) VALUES(?, ?)";
static NSString *const INSERT_BUSINESS_TABLE_SQL = @"INSERT INTO MPBusinessTable (userId,urlId,createtime) VALUES(?, ?, ?)";


static NSString *const SELECT_USERMODEL_WHERE_SQL = @"SELECT * from MPUserTable where email = ? ";
static NSString *const SELECT_URLMODEL_WHERE_SQL = @"SELECT * from MPUrlTable where urlString = ? ";
static NSString *const SELECT_BUSINESSMODEL_WHERE_SQL = @"SELECT * from MPBusinessTable where userId = ? and urlId = ? ";

static NSString *const SELECT_USERMODEL_NOT_STAR_SQL =
@"SELECT * FROM MPUserTable WHERE userId not in\
(select userId from MPBusinessTable a,MPUrlTable b WHERE a.urlId = ?)\
ORDER BY userId LIMIT 1 OFFSET 0\
";
@interface MPDataBaseManager ()
@property (strong, nonatomic) FMDatabaseQueue *dbQueue;
@end

@implementation MPDataBaseManager

#pragma mark - init
+ (instancetype)sharedManager {
    static id singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[[self class] alloc] init];
    });
    return singleton;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
//        [self setupDataBaseDefault];
    }
    return self;
}

- (void)setupDataBaseDefault {
    NSString * dbPath = [MPResourcePath stringByAppendingPathComponent:DB_NAME];
    debugLog(@"dbPath = %@", dbPath);
    _dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    [self createTables];
}

- (void)setupDataBaseWith:(NSString *)path {
    NSString * dbPath = [path stringByAppendingPathComponent:DB_NAME];
    debugLog(@"dbPath = %@", dbPath);
    _dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    [self createTables];
}

- (void)createTables {
    [_dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:CREATE_USER_TABLE_SQL];
        [db executeUpdate:CREATE_URL_TABLE_SQL];
        [db executeUpdate:CREATE_BUSINESS_TABLE_SQL];
    }];
}

#pragma mark - Store
#pragma mark 存储各种单个数据的类型
- (void)storeUser:(MPUserModel *)userModel complete:(void(^)(BOOL success ,NSError *error))complete {
    
    NSArray *userModels = [self selectUserModelByEmail:userModel.email];
    if (userModels.count) {
        //        debugLog(@"ERROR, there are repeated email");
        complete(NO, [NSError errorWithDomain:@"ERROR, there are repeated email" code:0 userInfo:nil]);
        return;
    }
    
    __block BOOL result;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        result = [db executeUpdate:INSERT_USER_TABLE_SQL,userModel.username,userModel.email,userModel.password,@([[NSDate date] timeIntervalSince1970])];
    }];
    if (result) {
        //        debugLog(@"ERROR, failed to insert User");
        complete(YES, nil);
    }
    else
    {
        complete(NO, [NSError errorWithDomain:@"ERROR, failed to insert User" code:0 userInfo:nil]);
    }
}

- (void)storeUrl:(MPUrlModel *)urlModel complete:(void(^)(BOOL success ,NSError *error))complete {

    NSArray *urlModels = [self selectUrlModelByUrlString:urlModel.urlString];
    if (urlModels.count) {
//        debugLog(@"ERROR, there are repeated urlString");
        complete(YES, [NSError errorWithDomain:@"ERROR, there are repeated url" code:0 userInfo:nil]);
        return;
    }
    
    __block BOOL result;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        result = [db executeUpdate:INSERT_URL_TABLE_SQL,urlModel.urlString,@([[NSDate date] timeIntervalSince1970])];
    }];
    if (result) {
        //        debugLog(@"ERROR, failed to insert URL");
        complete(YES, nil);
    }
    else
    {
        complete(NO, [NSError errorWithDomain:@"ERROR, failed to insert URL" code:0 userInfo:nil]);
    }
}

- (void)storeBusiness:(MPBusinessModel *)businessModel complete:(void(^)(BOOL success ,NSError *error))complete {
    __block BOOL result;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        result = [db executeUpdate:INSERT_BUSINESS_TABLE_SQL,@(businessModel.userId),@(businessModel.urlId),@([[NSDate date] timeIntervalSince1970])];
    }];
    
    if (result) {
        complete(YES, nil);
    }
    else
    {
        complete(NO, [NSError errorWithDomain:@"ERROR, failed to insert business" code:0 userInfo:nil]);
    }
}

- (void)storeBusiness:(NSString *)email url:(NSString *)url complete:(void(^)(BOOL success ,NSError *error))complete {
    // 1.根据email地址查询得到userId
   NSArray<MPUserModel *> *userModels = [self selectUserModelByEmail:email];
    
    if (!userModels.count) {
         complete(NO, [NSError errorWithDomain:@"ERROR, not have this email" code:0 userInfo:nil]);
        return;
    }
    NSUInteger userId = [userModels firstObject].userId;
    // 2.存url并查询urlId
    MPUrlModel *urlModel = [MPUrlModel new];
    urlModel.urlString = url;
    [self storeUrl:urlModel complete:^(BOOL success, NSError *error) {
        if(success)
        {
            NSArray<MPUrlModel *> *urlModels = [self selectUrlModelByUrlString:url];
            if (!urlModels.count) {
                complete(NO, [NSError errorWithDomain:@"ERROR, not have this url" code:0 userInfo:nil]);
                return;
            }
            NSUInteger urlId = [urlModels firstObject].urlId;
            
            // 3.存到业务表
            MPBusinessModel *businessModel = [MPBusinessModel new];
            businessModel.userId = userId;
            businessModel.urlId = urlId;
            [self storeBusiness:businessModel complete:complete];
        }
        else {
            complete(success, error);
        }
    }];
}

- (NSArray<MPUserModel *> *)selectUserModelByEmail:(NSString *)email {
    NSMutableArray *datas = [NSMutableArray array];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:SELECT_USERMODEL_WHERE_SQL,email];
        while ([rs next]) {
            MPUserModel *userModel = [MPUserModel new];
            userModel.userId = [rs intForColumn:@"userId"];
            userModel.username = [rs stringForColumn:@"username"];
            userModel.email = [rs stringForColumn:@"email"];
            userModel.password = [rs stringForColumn:@"password"];
            userModel.createtime = [rs doubleForColumn:@"createtime"];
            [datas addObject:userModel];
        }
        [rs close];
    }];
    return [NSArray arrayWithArray:datas];
}

- (NSArray<MPUrlModel *> *)selectUrlModelByUrlString:(NSString *)urlString {
    NSMutableArray *datas = [NSMutableArray array];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:SELECT_URLMODEL_WHERE_SQL,urlString];
        while ([rs next]) {
            MPUrlModel *urlModel = [MPUrlModel new];
            urlModel.urlId = [rs intForColumn:@"urlId"];
            urlModel.urlString = [rs stringForColumn:@"urlString"];
            urlModel.createtime = [rs doubleForColumn:@"createtime"];
            [datas addObject:urlModel];
        }
        [rs close];
    }];
    return [NSArray arrayWithArray:datas];
}

- (NSArray<MPBusinessModel *> *)selectBusinessModelByUserId:(NSUInteger )userId urlId:(NSUInteger )urlId {
    NSMutableArray *datas = [NSMutableArray array];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:SELECT_BUSINESSMODEL_WHERE_SQL,@(userId),@(urlId)];
        while ([rs next]) {
            MPBusinessModel *urlModel = [MPBusinessModel new];
            urlModel.businessId = [rs intForColumn:@"businessId"];
            urlModel.urlId = [rs intForColumn:@"urlId"];
            urlModel.userId = [rs intForColumn:@"userId"];
            urlModel.createtime = [rs doubleForColumn:@"createtime"];
            [datas addObject:urlModel];
        }
        [rs close];
    }];
    return [NSArray arrayWithArray:datas];
}


- (void)getUserModelByUrlString:(NSString *)urlString complete:(void(^)(MPUserModel *userModel, NSError *error))complete {
    
//    NSArray<MPUrlModel *> *urlModels = [self selectUrlModelByUrlString:urlString];
//    if (!urlModels.count) {
//        complete(nil, [NSError errorWithDomain:@"ERROR, not have this url" code:0 userInfo:nil]);
//        return;
//    }
    MPUrlModel *urlModel = [MPUrlModel new];
    urlModel.urlString = urlString;
    
    [self storeUrl:urlModel complete:^(BOOL success, NSError *error) {
        if (success) {
            NSArray<MPUrlModel *> *urlModels = [self selectUrlModelByUrlString:urlString];
            if (!urlModels.count) {
                complete(nil, [NSError errorWithDomain:@"ERROR, not have this url" code:0 userInfo:nil]);
                return;
            }
        
            NSUInteger urlId = [urlModels firstObject].urlId;
        
            NSArray<MPUserModel *> *userModels = [self selectUserModelNoStar:urlId];
        
            if (userModels.count) {
                complete([userModels firstObject], nil);
            }
            else{
                complete(nil, [NSError errorWithDomain:@"ERROR, not have more user" code:0 userInfo:nil]);
            }
        }
        else {
            complete(nil, error);
        }
    }];
    
//    NSUInteger urlId = [urlModels firstObject].urlId;
//    
//    NSArray<MPUserModel *> *userModels = [self selectUserModelNoStar:urlId];
//    
//    if (userModels.count) {
//        complete([userModels firstObject], nil);
//    }
//    else{
//        complete(nil, [NSError errorWithDomain:@"ERROR, not have more user" code:0 userInfo:nil]);
//    }
    
//    NSArray<MPUserModel *> *userModels = [self selectUserModelByEmail:email];
//    if (!userModels.count) {
//        complete(nil, [NSError errorWithDomain:@"ERROR, not have this email" code:0 userInfo:nil]);
//        return;
//    }
//    NSUInteger userId = [userModels firstObject].userId;
//    NSArray<MPBusinessModel *> *businessModels = [self selectBusinessModelByUserId:userId urlId:urlId];
//    if (!businessModels.count) {
//        complete([userModels firstObject], nil);
//    }
}

- (NSArray<MPUserModel *> *)selectUserModelNoStar:(NSUInteger )urlId {
    NSMutableArray *datas = [NSMutableArray array];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:SELECT_USERMODEL_NOT_STAR_SQL,@(urlId)];
        while ([rs next]) {
            MPUserModel *userModel = [MPUserModel new];
            userModel.userId = [rs intForColumn:@"userId"];
            userModel.username = [rs stringForColumn:@"username"];
            userModel.email = [rs stringForColumn:@"email"];
            userModel.password = [rs stringForColumn:@"password"];
            userModel.createtime = [rs doubleForColumn:@"createtime"];
            [datas addObject:userModel];
        }
        [rs close];
    }];
    return [NSArray arrayWithArray:datas];
}

- (NSArray<MPUserModel *> *)getAllUserModel {
    NSMutableArray *datas = [NSMutableArray array];
    NSString *SQL = [NSString stringWithFormat:SELECT_ALL_SQL,USER_TABLE_NAME];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:SQL];
        while ([rs next]) {
            MPUserModel *userModel = [MPUserModel new];
            userModel.userId = [rs intForColumn:@"userId"];
            userModel.username = [rs stringForColumn:@"username"];
            userModel.email = [rs stringForColumn:@"email"];
            userModel.password = [rs stringForColumn:@"password"];
            userModel.createtime = [rs doubleForColumn:@"createtime"];
            [datas addObject:userModel];
        }
        [rs close];
    }];
    return [NSArray arrayWithArray:datas];
}

@end

@implementation MPUserModel
NSString * const MPRegisterPassword = @"ilovecode2017";
#define MPNamePath [MPResourcePath stringByAppendingString:@"/FamilyNames.txt"]
#define MPFailAccountPath [MPResourcePath stringByAppendingString:@"/RegisterFailEmailList.txt"]
#define MPEmailServerPath [MPResourcePath stringByAppendingString:@"/EmailServerList.txt"]

static NSArray *names;
static NSArray *emailServerList;
+ (instancetype)random {
    NSString *newName = [[self class] getUsername];
    NSString *emailSuffix = [[self class] getEmailSuffix];
    MPUserModel *registerModel = [MPUserModel new];
    registerModel.username = newName;
    registerModel.email = [registerModel.username stringByAppendingString:emailSuffix];
    registerModel.password = [self getPassword];
    return registerModel;
}

+ (NSString *)getPassword{
//    return [self generatePassword];
    return MPRegisterPassword;
}

+ (NSString *)getUsername
{
    NSString *familyName = [[self class] getFamilyName];
    NSString *username =[[familyName stringByAppendingString:@"-"] stringByAppendingString:[[self class] generateNameSuffix]];
    return username;
}

+ (NSString *)getFamilyName {
    int index =  arc4random() % names.count;
    NSString *familyName = [names objectAtIndex:index];
    return familyName;
}


/**
 *  获取Email邮箱服务器后缀包括@符号
 *
 *  @return getEmailSuffix
 */
+ (NSString *)getEmailSuffix {
    int index =  arc4random() % emailServerList.count;
    NSString *emailServer = [emailServerList objectAtIndex:index];
    
    return [NSString stringWithFormat:@"@%@",emailServer];
}

+ (void)initialize {
    NSString *namePath = [[NSBundle mainBundle] pathForResource:@"FamilyNames" ofType:@"txt"];
    NSString * nameString = [NSString stringWithContentsOfFile:namePath encoding:NSUTF8StringEncoding error:nil];
    //[NSString stringWithContentsOfFile:MPNamePath encoding:NSUTF8StringEncoding error:nil];
    names = [nameString componentsSeparatedByString:@"\n"];
    NSString *emailPath = [[NSBundle mainBundle] pathForResource:@"EmailServerList" ofType:@"txt"];
    NSString *emailServerString = [NSString stringWithContentsOfFile:emailPath encoding:NSUTF8StringEncoding error:nil];
    //[NSString stringWithContentsOfFile:MPEmailServerPath encoding:NSUTF8StringEncoding error:nil];
    emailServerList = [emailServerString componentsSeparatedByString:@"\n"];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"[%@ %@ %@]",self.username,self.email,self.password];
}
- (void)saveSuccessModel {
    NSLog(@"%@",self);
    [[MPDataBaseManager sharedManager] storeUser:self complete:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"注册并保存成功:%@",self.email);
        }
        else
        {
            NSLog(@"注册成功保存失败：%@",error);
        }
    }] ;
}

- (void)saveRegisterFailModel{
  #ifdef DEBUG
        NSString *writeString = [self.email stringByAppendingString:@"\n"];
        NSData *writeDate = [writeString dataUsingEncoding:NSUTF8StringEncoding];
        [[MPFileManager shareInstance] writeFile:MPFailAccountPath data:writeDate writeMode:MPFileManagerWriteModeAdd complete:^(BOOL result) {
            //        NSLog(@"保存成功");
            //        NSHomeDirectory()
        }];
#endif
}

+ (NSString *)generateNameSuffix {
    static int kNumber = 3;
    
    NSString *sourceStr = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //    NSString *sourceStr = @"abcdefghijklmnopqrstuvwxyz";
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    //    srand((unsigned)time(0));
    for (int i = 0; i < kNumber; i++) {
        unsigned index = arc4random() % [sourceStr length];
        NSString *oneStr = [sourceStr substringWithRange:NSMakeRange(index, 1)];
        [resultStr appendString:oneStr];
    }
    return resultStr;
}

@end

@implementation MPUrlModel
@end

@implementation MPBusinessModel
@end
