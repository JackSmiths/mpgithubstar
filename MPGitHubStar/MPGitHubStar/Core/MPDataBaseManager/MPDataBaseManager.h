

#ifdef DEBUG
#define debugLog(...)    NSLog(__VA_ARGS__)
#define debugMethod()    NSLog(@"%s", __func__)
#define debugError()     NSLog(@"Error at %s Line:%d", __func__, __LINE__)
#else
#define debugLog(...)
#define debugMethod()
#define debugError()
#endif

#define MPResourcePath [[[NSBundle mainBundle] infoDictionary] objectForKey:@"ResourceDir"]


#import <Foundation/Foundation.h>
#import "FMResultSet.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"



extern  NSString *const DB_NAME ;
extern  NSString *const TIME_TABLE_TYPE_DISCONNECT ;

@class MPUserModel;
@class MPUrlModel;
@class MPBusinessModel;
@interface MPDataBaseManager : NSObject

@property (strong, readonly, nonatomic) FMDatabaseQueue *dbQueue;

+ (instancetype)sharedManager;

- (void)setupDataBaseDefault;
- (void)setupDataBaseWith:(NSString *)path;
- (void)storeUser:(MPUserModel *)userModel complete:(void(^)(BOOL success ,NSError *error))complete;
- (void)storeUrl:(MPUrlModel *)urlModel complete:(void(^)(BOOL success ,NSError *error))complete;
- (void)storeBusiness:(NSString *)email url:(NSString *)url complete:(void(^)(BOOL success, NSError *error))complete;
- (void)getUserModelByUrlString:(NSString *)urlString complete:(void(^)(MPUserModel *userModel, NSError *error))complete;

- (NSArray<MPUserModel *> *)getAllUserModel;

@end

@interface MPUserModel : NSObject
@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, assign) NSTimeInterval createtime;

+ (instancetype)random;
- (void)saveSuccessModel;
- (void)saveRegisterFailModel;
@end

@interface MPUrlModel : NSObject
@property (nonatomic, assign) NSUInteger urlId;
@property (nonatomic, copy) NSString *urlString;
@property (nonatomic, assign) NSTimeInterval createtime;
@end

@interface MPBusinessModel : NSObject
@property (nonatomic, assign) NSUInteger businessId;
@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, assign) NSUInteger urlId;
@property (nonatomic, assign) NSTimeInterval createtime;
@end
